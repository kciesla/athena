/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/********
Author Caleb Lampen <lampen@physics.arizona.edu>
University of Arizona
***/
#include "CscCalibData/CscCalibResultCollection.h"

const std::string& CscCalibResultCollection::parName() const { return m_parName;}
